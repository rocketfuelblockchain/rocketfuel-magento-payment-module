<?php

namespace RKFL\Rocketfuel\Model;

use Magento\Framework\HTTP\Client\Curl as MagentoCurl;


class Curl
{
    protected $curl;
    /**
     * constructor.
     * @param MagentoCurl $curl
     */
    public function __construct(MagentoCurl $curl)
    {

        $this->curl = $curl;
        $this->curl->addHeader("Content-Type", "application/json");
        // $this->curl->addHeader("Content-Length", 200);

    }

    /**
     * Swap OrderId to new orderId 
     * @param array $data
     */
    public function swapOrderId($data)
    {
        $body = $data['body'];

        $url = $data['endpoint'] . '/update/orderId';


        $this->curl->post($url, $body);

        $result = $this->curl->getBody();

        return $result;
    }

    /**
     * Process data to get uuid
     *
     * @param array $data - Data from plugin.
     */
    public function processPayment($data)
    {

        $response = $this->auth($data);

        $result = json_decode($response);

        if (!isset($result->ok) || $result->ok !== true || !$result->result->access) {

            return array(
                'success' => false,
                'message' => 'Authorization cannot be completed'
            );
        }


        if (!$result) {

            return array(
                'success' => false,
                'message' => 'Authorization cannot be completed'
            );
        }

        $charge_response = $this->createCharge($result->result->access, $data);

        $charge_result = json_decode($charge_response);

        if (!$charge_result || $charge_result->ok === false) {

            return array('success' => false, 'message' => 'Could not establish an order: ' . $charge_result->message);
        }

        return $charge_result;
    }

    /**
     * Process authentication
     * @param array $data
     */
    public function auth($data)
    {
        $body = json_encode($data['cred']);

        $url = $data['endpoint'] . '/auth/login';


        $this->curl->post($url, $body);

        $result = $this->curl->getBody();

        return $result;
    }

    /**
     * Get UUID of the customer
     * @param string $accessToken
     * @param array $data
     */
    public function createCharge($accessToken, $data)
    {

        $body = $data['body'];

        //  Check partial transaction case
        $previousOrder = json_decode($_POST['previousOrder'], true);
        if($previousOrder && $previousOrder['uuid'] && intval($_POST['checkPartial'])) {
            $this->curl->addHeader('authorization', "Bearer  $accessToken");
            $url = $data['endpoint'] . '/purchase/transaction/partials/'. $body['merchant_id'].'?offerId='.$previousOrder['temporary_order_id'].'&hostedPageId='.$previousOrder['uuid'];
            $this->curl->get($url);
            $purchasePartialsResult = $this->curl->getBody();
            $purchasePartialsResult = json_decode($purchasePartialsResult, true);
            if( isset( $purchasePartialsResult['result']['tx'] ) 
                && !is_null( $purchasePartialsResult['result']['tx'] ) 
                && (int)$purchasePartialsResult['result']['tx']['status'] === 101 
                && (int)$purchasePartialsResult['result']['paymentLinkStatus'] === 1 
                && self::isCartSame($purchasePartialsResult['result']['tx'], $body)){
                return json_encode(
                    array(
                        "ok" => true,
                        "result" => array(
                            "isPartial" => true,
                            "uuid" => $previousOrder['uuid'],
                            "temporary_order_id" => $previousOrder['temporary_order_id']
                        )
                    )
                );
            }
        }
        $this->curl->addHeader('authorization', "Bearer  $accessToken");

        $url = $data['endpoint'] . '/hosted-page';

        $this->curl->post($url, json_encode($body));

        // output of curl request
        $result = $this->curl->getBody();
        return $result;
    }

    public static function isCartSame( $preTransaction, $currentTransaction ){
           
        $preCartInfo = $preTransaction['check'];

        if( !is_array( $preCartInfo ) ){
            return false;
        }

        if( $preTransaction['nativeAmount'] < $currentTransaction['amount'] ){
            return false;
        }
        
        if( count( $preCartInfo ) !== count( $currentTransaction['cart'] ) ){
            return false;

        }
      
        $prevCartIdArr = array_map( function( $element ){
      
            return (string)$element['id'];

        }, $preCartInfo );

  
        $flag_incompatible_cart_product = false;

        foreach( $currentTransaction['cart'] as  $cart_item ){
            $is_product_present = array_search( (string)$cart_item->id, $prevCartIdArr);    
            if( $is_product_present === false ){
                $flag_incompatible_cart_product = true;            
            }

        }
        if( $flag_incompatible_cart_product === true ){
            return false;
        }

        return true;
    }
}
